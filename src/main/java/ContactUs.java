/*

Rombie Lagunzad
CIT 360
Brother Lawrence

When this servlet is called, it will handle a simple request by creating an HTML content response which
displays the data input by the user as a summary receipt.

 */

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ContactUs", urlPatterns={"/ContactUs"})
public class ContactUs extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head><link href=\"css/style.css\" rel=\"stylesheet\"></head><body>");
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String message = request.getParameter("message");
        out.println("<h1>Thank you for contacting us, " + name + "!</h1>");
        out.println("<h2>Here's your message receipt</h2>");
        out.println("<p>Name: " + name + "</p>");
        out.println("<p>Email: " + email + "</p>");
        out.println("<p>Phone: " + phone + "</p>");
        out.println("<p>Message: " + message + "</p>");
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><head><link href=\"css/style.css\" rel=\"stylesheet\"></head><body>");
        out.println("<h1>Page Not Found</h2>");
        out.println("<a class=\"click-here\" href=\"http://localhost:8080/week7_war_exploded/index.html\">Go Back to Home Page</a>");
        out.println("</body></html>");
    }
}
